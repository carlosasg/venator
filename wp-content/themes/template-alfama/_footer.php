<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage None Plate
 * @since None Plate 1.0
 */
?>


<!-- Footer Links Libraries -->
	<!-- Prefix Free -->
	<script type='text/javascript' src='<?php echo get_template_directory_uri(); ?>/assets/libs/prefixfree/prefixfree.min.js'></script>

	<!-- Alertify -->
	<script type='text/javascript' src='<?php echo get_template_directory_uri(); ?>/assets/libs/alertify/js/alertify.js'></script>

	<!-- Lightbox -->
	<script type='text/javascript' src='<?php echo get_template_directory_uri(); ?>/assets/libs/lightbox/js/lightbox.min.js'></script>

	<!-- Lince Form -->
	<script type='text/javascript' src='<?php echo get_template_directory_uri(); ?>/assets/libs/linceform/linceform.js'></script>

	<!-- Bootstrap -->
	<!-- <script type='text/javascript' src='<?php echo get_template_directory_uri(); ?>/assets/libs/bootstrap/bootstrap.min.js'></script> -->
    <script src="<?php echo get_template_directory_uri(); ?>/assets/js/bootstrap.min.js"></script>

	<!--  Cycle 2  -->
	<script type='text/javascript' src='<?php echo get_template_directory_uri(); ?>/assets/libs/cycle2/jquery.cycle2.min.js'></script>
	<!-- <script type='text/javascript' src='<?php echo get_template_directory_uri(); ?>/assets/libs/cycle2/jquery.cycle2.carousel.min.js'></script> -->
	<script type='text/javascript' src='<?php echo get_template_directory_uri(); ?>/assets/libs/cycle2/jquery.cycle2.swipe.min.js'></script>

	<!--  if IE 8  -->
	<script type='text/javascript' src='http://r2server.com.br/plugins/oldbrowsers/ifIE.js'></script>

	<!-- Main JS -->
	<script type='text/javascript' src='<?php echo get_template_directory_uri(); ?>/assets/js/app.js'></script>


	<script src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery-3.2.1.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/js/popper.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/js/owl.carousel.min.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/instafeed.min.js"></script>


<!-- Footer Tags -->
	<?php wp_footer(); ?>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Woohoo, you're reading this text in a modal!</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
            </div>
        </div>
    </div>


	<script type="text/javascript">
      var feed = new Instafeed({
        accessToken: 'IGQVJYZAG5qUElxbmR4dnZArXzRQT2wzR19qUEdkNEtwdGJ6VDJyLUhOX0JEUWJibEp0bVYtS0hjR2dtemQyekNjT0t6bGZAnMDFMa29mSDhsbG5vSFg3c0sxdmlod0xfY0hxUnItaXZAvc3BycnRndlZABYwZDZD',
        limit: 6,
        template: '<div class="item"> <a href="{{link}}" target="_blank"><img title="{{caption}}" src="{{image}}" /></a></div>',
          after: function(){
            $('.galeria-insta').owlCarousel({
                loop:true,
                margin:0,
                padding:0,
                dots:false,
                nav:false,
                responsive:{
                    0:{
                        items:1
                    },
                    600:{
                        items:1
                    },
                    1000:{
                        items:6
                    }
                }
            })
          }
      });

      feed.run();
  </script>
  <script>      
      // CAROUSEL
      $('.display-slider').owlCarousel({
          loop:true,
          margin:0,
          padding:0,
          dots:true,
          nav:true,
          responsive:{
              0:{
                  items:1
              },
              600:{
                  items:1
              },
              1000:{
                  items:1
              }
          }
      })

      $('.galeria-imagens').owlCarousel({
          loop:true,
          margin:0,
          padding:0,
          dots:false,
          nav:true,
          responsive:{
              0:{
                  items:1
              },
              600:{
                  items:1
              },
              1000:{
                  items:1
              }
          }
      })

      $('.galeria-produtos').owlCarousel({
          loop:true,
          margin:15,
          padding:0,
          dots:false,
          nav:true,
          responsive:{
              0:{
                  items:1
              },
              600:{
                  items:1
              },
              1000:{
                  items:1
              }
          }
      })


      $('.anchor-link')
        .click(function(event) {
            // On-page links
            if (
            location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') 
            && 
            location.hostname == this.hostname
            ) {
            // Figure out element to scroll to
            
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            // Does a scroll target exist?
            if (target.length) {
                // Only prevent default if animation is actually gonna happen
                if ($(window).width()<768){
                    $('header .navbar-toggler').trigger('click')
                }
                event.preventDefault();
                $('html, body').animate({
                scrollTop: target.offset().top
                }, 800, function() {
                });
            }
            }
        });


        $(document).ready(function(){
            $('#exampleModal').modal('show')
        });


    </script>
    
</body>
</html>
