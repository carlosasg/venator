<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage None Plate
 * @since None Plate 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>


<!-- Head Tags -->
    <!-- Meta Data -->
    <meta charset="<?php bloginfo( 'charset' ); ?>">

    <!-- Viewport  -->
    <meta name='viewport' content='width=device-width, initial-scale=1.0, user-scalable=no'>

    <!-- Favicon -->
    <link rel='shortcut icon' type='image/x-icon' href='<?php echo get_template_directory_uri(); ?>/assets/img/meta/favicon-32x32.png'>

    <!-- Meta -->
    <meta name="description" content="<?php echo get_bloginfo( 'description' ); ?>">

    <!-- Wordpress Shit -->
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <?php endif; ?>
    <?php wp_head(); ?>


<!-- Head Links Libraries -->
    <!-- Alertify -->
    <link rel='stylesheet' type='text/css' href='<?php echo get_template_directory_uri(); ?>/assets/libs/alertify/css/alertify.core.css'>
    <link rel='stylesheet' type='text/css' href='<?php echo get_template_directory_uri(); ?>/assets/libs/alertify/css/alertify.default.css'>

    <!-- Lightbox -->
    <link rel='stylesheet' type='text/css' href='<?php echo get_template_directory_uri(); ?>/assets/libs/lightbox/css/lightbox.css'>

<!-- Font Awesome -->
<!-- <link rel='stylesheet' type='text/css'
        href='<?php echo get_template_directory_uri(); ?>/assets/libs/fontawesome/css/font-awesome.min.css'> -->
            <!-- Fontawesome -->
    <script async src="https://kit.fontawesome.com/b29611fbd4.js"></script>

    <!-- Bootstrap -->
    <!-- <link rel='stylesheet' type='text/css' href='<?php echo get_template_directory_uri(); ?>/assets/libs/bootstrap/bootstrap.min.css'> -->
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/css/bootstrap.min.css">

    <!-- Normalize -->
    <link rel='stylesheet' type='text/css' href='<?php echo get_template_directory_uri(); ?>/assets/libs/normalize/normalize.css'>

    <!-- Lince Form -->
    <link rel='stylesheet' type='text/css' href='<?php echo get_template_directory_uri(); ?>/assets/libs/linceform/linceform.css'>

    <!-- jQuery -->
    <script type='text/javascript' src='<?php echo get_template_directory_uri(); ?>/assets/libs/jquery/jquery-1.11.1.min.js'></script>

    <!-- Wow -->
    <link rel='stylesheet' type='text/css' href='<?php echo get_template_directory_uri(); ?>/assets/libs/wow/animate.min.css'>

    <!-- Main CSS -->
    <link rel='stylesheet' href='<?php echo get_template_directory_uri(); ?>/assets/css/css/style.css'>

    <!-- lightcase -->
    <link rel='stylesheet' type='text/css' href='<?php echo get_template_directory_uri(); ?>/assets/libs/lightcase/css/lightcase.css'>

    <!--  Owl Carousel  -->
    <link rel='stylesheet prefetch' href='<?php echo get_template_directory_uri(); ?>/assets/libs/owl/owl.carousel.css'>
    <link rel='stylesheet prefetch' href='<?php echo get_template_directory_uri(); ?>/assets/libs/owl/owl.theme.default.css'>

    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/css/owl.theme.default.min.css">
    
    <!-- Modernizr  -->
    <script type='text/javascript' src='<?php echo get_template_directory_uri(); ?>/assets/libs/modernizr/modernizr.js'></script>

    <!-- HTML5 Shiv -->
    <script type='text/javascript' src='<?php echo get_template_directory_uri(); ?>/assets/libs/html5shiv/html5shiv.min.js'></script>

    <!-- Google Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">

</head>





<body <?php body_class(); ?>>

    <!-- <div class="loading">
        <p><i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw margin-bottom"></i></p>
    </div> -->
    <!-- RESPONSIVE MENU -->
    <header>
    <nav class="navbar navbar-expand-lg">
        <div class="container">
          <a class="navbar-brand" href="/index.php"><img src="wp-content/themes/template-alfama/assets/img/logo-venator.png" class="logo-topo" alt="Venator - Charutaria & Lounge"></a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample07" aria-controls="navbarsExample07" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"><i class="fas fa-bars"></i></span>
          </button>
  
          <div class="collapse navbar-collapse" id="navbarsExample07">
            <ul class="navbar-nav ml-auto">
              <li class="nav-item">
                <a class="nav-link item-menu anchor-link" href="#sobre">A VENATOR</a>
              </li>
              <li class="nav-item">
                <a class="nav-link item-menu anchor-link" href="#galeria">LOUNGE E AFINS</a>
              </li>
              <li class="nav-item">
                <a class="nav-link item-menu anchor-link" href="#produtos">SOMMELIER</a>
              </li>
              <li class="nav-item">
                <a class="nav-link item-menu anchor-link" href="#club">VENATOR CLUB</a>
              </li>
              <li class="nav-item">
                <a class="nav-link item-menu anchor-link" href="#footer">CONTATO</a>
              </li>
            </ul>
            <ul class="navbar-nav ml-auto social-menu-mobile">
              <li class="nav-item">
                <a class="nav-link item-menu anchor-link social-top-menu" href="https://www.facebook.com/venatorcharutarialounge/" target="_blank"><i class="fab fa-facebook-f"></i></a>
              </li>
              <li class="nav-item">
                <a class="nav-link item-menu anchor-link social-top-menu" href="https://www.youtube.com/channel/UCV2NZO6T46A54t45q4rxTbw" target="_blank"><i class="fab fa-youtube"></i></a>
              </li>
              <li class="nav-item">
                <a class="nav-link item-menu anchor-link social-top-menu" href="https://www.instagram.com/venatorcharutarialounge/" target="_blank"><i class="fab fa-instagram"></i></a>
              </li>
            </ul>
            <ul class="navbar-nav ml-auto">
              <li class="nav-item btn-pedido-top-menu">
              <a class="nav-link item-menu anchor-link " href="https://venator.goomer.app/" target="_blank">FAÇA SEU PEDIDO</a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
  </header>
