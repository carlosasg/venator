<?php
/** 
* Template Name: Início1
**/
?>

<?php get_header(); ?>

<meta property='og:image' content='<?php echo $post_thumbnail_url; ?>'/>
<div id="content" class="">

<!-- Display -->
<section id="display">
    <div class="owl-carousel owl-theme display-slider">
        <?php 
            // WP QUERY
            query_posts( array(
                    'post_type' => 'display',
                    'posts_per_page' => 4,
                    'posts_per_rss' => 4,
                    'orderby'=> 'date&order=ASC',
                )); 

        ?>
        <?php 
            // LOOP
            if ( have_posts() ) :
                while ( have_posts() ) : the_post();
        ?>
            <?php
                $categoryclass = '';
                $categories = get_the_category();
                foreach ( $categories as $category ) { 
                    $categoryclass .= ' ct-'.esc_attr( $category->slug ); 
                }
            ?>

                    <div class="item bg-display" style="background-image: url(<?php echo the_field('imagem') ?>);">
                        <h3><?php echo the_field('sub-titulo') ?></h3> 
                        <h2><?php echo the_field('descricao') ?></h2>
                        <div class="pelicula-escura"></div>
                    </div>
                    
                    <?php
                endwhile;
                else :
                    ?>
            <p>Nada foi encontrado</p>
            <?php
            endif;
            ?>
        <?php wp_reset_query(); ?>

</div>
</section>


<!-- Sobre -->
<section id="sobre" style="background-image: url(wp-content/themes/template-alfama/assets/img/sobre-bg.png);" >
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
           
                <h1><?php the_field('titulo') ?></h1>
                <p><?php the_field('sobre') ?></p>
                </div>
                <div class="col-sm-8">
                  <iframe width="100%" height="410px" style="position:absolute;top:0;left:0;border-radius:20px;" src="<?php the_field('vimeo') ?>" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

            </div>
        </div>
    </div>
</section>


<!-- GALERIAS DESK-->
<?php
$array = [];
$images = get_field('galeria');
$contador = 0;

if ($images) {
    foreach($images as $image) {
        $array[] = $image;
    }
}

?>
       
        <section id="galeria" class="hidden-mob">
            <div class="owl-carousel galeria-imagens owl-theme">
                
                <?php do { ?>

                <div class="item">
                    <div class="container-fluid">
                        <div class="row">
                            <a href="<?php if ($contador>=count($array)) {$contador=0;}; echo $array[$contador]; ?>" class="foobox" style="width: 50%;display: contents;">
                              <div class="col-sm-6 galeria-1" style="background-image: url(<?php echo $array[$contador]; $contador++; ?>);">
                              </div>
                            </a>
                            <div class="col-sm-6">
                                <div class="row">
                                    <a href="<?php if ($contador>=count($array)) {$contador=0;}; echo $array[$contador];?>" class="foobox" style="width: 50%;display: contents;">
                                      <div class="col-sm-6 galeria-2 foobox" style="background-image: url(<?php echo $array[$contador]; $contador++; ?>);"></div>
                                    </a>
                                    <a href="<?php if ($contador>=count($array)) {$contador=0;}; echo $array[$contador];?>" class="foobox" style="width: 50%;display: contents;">
                                      <div class="col-sm-6 galeria-3 foobox" style="background-image: url(<?php echo $array[$contador]; $contador++; ?>);"></div>
                                    </a>
                                    <a href="<?php if ($contador>=count($array)) {$contador=0;}; echo $array[$contador];?>" class="foobox" style="width: 50%;display: contents;">
                                      <div class="col-sm-12 galeria-4 foobox" style="background-image: url(<?php echo $array[$contador]; $contador++; ?>);"></div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <?php } while ($contador < count($array)) ?>
    </div>
</section>

<!-- GALERIAS MOB-->
<?php
$array = [];
$images = get_field('galeria');
$contador = 0;

if ($images) {
    foreach($images as $image) {
        $array[] = $image;
    }
}

?>
       
        <section id="galeria" class="hidden-desk">
            <div class="owl-carousel galeria-imagens owl-theme">
                
                <?php do { ?>

                <div class="item">
                    <div class="container-fluid">
                        <div class="row">
                          <a href="<?php if ($contador>=count($array)) {$contador=0;}; echo $array[$contador];?>" class="foobox" style="width: 50%;display: contents;">
                            <div class="col-sm-12 galeria-1" style="background-image: url(<?php echo $array[$contador]; $contador++; ?>);">
                            </div>
                          </a>
                        </div>
                    </div>
                </div>

                <?php } while ($contador < count($array)) ?>
    </div>
</section>

<section id="produtos"  style="background-image: url(wp-content/themes/template-alfama/assets/img/sobre-bg.png);">
        <img src="wp-content/themes/template-alfama/assets/img/tabaco.png" class="tabaco" alt="">
          <div class="container">
            <div class="row">
              <div class="col-sm-12">
                <div class="row">
                  <div class="col-sm-5">

                        <div class="owl-carousel owl-theme galeria-produtos">
                          <?php
                              $images = get_field('galeria_somelier');
                              
                              if ($images) {
                                foreach($images as $image) {
                                  ?>
                                    <div class="item">
                                        <div class="col-sm-12 produto" style="background-image: url(<?php echo $image ?>)"></div>
                                    </div>
                                    <?php
                                  }
                              }

                              ?>
                        </div>
                      </div>
                      
                      <div class="col-sm-7 fundo-cinza">
                        <h4>CONHEÇA O NOSSO</h4>
                        <h3>CIGAR SOMMELIER </h3>
                        <H2><?php the_field('nome_somelier') ?></H2>
                        <p><?php the_field('texto_somelier') ?></p>
                      </div>


                    
                    
                  </div>
              </div>
            </div>
          </div>
          <img src="wp-content/themes/template-alfama/assets/img/moldura.png" class="moldura" alt="">
        </section>
 
        <section id="duvidas" class="bg-duvidas" style="background-image: url(wp-content/themes/template-alfama/assets/img/bg-duvidas.png);">
          <div class="container">
            <div class="row">
              <div class="col-sm-12 text-center mb-5">
                <h3>SOU UNICIANTE</h3>
                <h2>POR ONDE COMEÇAR?<h2>
              </div>
            </div>

            <?php 
                // WP QUERY
                query_posts( array(
                        'post_type' => 'iniciante',
                        'posts_per_page' => 4,
                        'posts_per_rss' => 4,
                        'orderby'=> 'date&order=ASC',
                    )); 

                ?>
                <?php 
                    // LOOP
                    if ( have_posts() ) :
                        while ( have_posts() ) : the_post();
                ?>
                    <?php
                        $categoryclass = '';
                        $categories = get_the_category();
                        foreach ( $categories as $category ) { 
                            $categoryclass .= ' ct-'.esc_attr( $category->slug ); 
                        }
                    ?>

            <div class="row">
              <div class="col-sm-5 mt-5 flex-inli">
                  <div class="row m-0-mob">
                    <div class="col-sm-8 t-end">
                      <h4>DICA 01</h4>
                      <p><?php the_field('dica1') ?></p>
                    </div>
                    <div class="col-sm-2 improviso1">
                      <img src="<?php the_field('imagem_dica_1') ?>" class="img-flid" alt="Dica 1">
                    </div>
                  </div>
                  <div class="altura-dica-mob">
                    <div class="row m-0-mob">
                      <div class="col-sm-8 t-end">
                        <h4>DICA 02</h4>
                        <p><?php the_field('dica2') ?></p>
                      </div>
                      <div class="col-sm-2 improviso1">
                        <img src="<?php the_field('imagem_dica_2') ?>" class="img-flid" alt="Dica 1">
                      </div>
                    </div>
                  </div>
              </div>
              <div class="col-sm-2">
                <img src="wp-content/themes/template-alfama/assets/img/charuto-duvida.png" class="img-fluid charuto-dicas" alt="">
              </div>
              <div class="col-sm-5 mt-5 flex-inli">
                  <div class="row m-0-mob">
                    <div class="col-sm-3 improviso1">
                      <img src="<?php the_field('imagem_dica_3') ?>" class="img-flid" alt="Dica 1">
                    </div>
                    <div class="col-sm-8 mt-30">
                      <h4>DICA 03</h4>
                      <p><?php the_field('dica3') ?></p>
                    </div>
                  </div>

                  <div class="altura-dica-mob">
                    <div class="row m-0-mob">
                      <div class="col-sm-3 improviso1">
                        <img src="<?php the_field('imagem_dica_4') ?>" class="img-flid" alt="Dica 1">
                      </div>
                      <div class="col-sm-8 mt-30">
                        <h4>DICA 04</h4>
                        <p><?php the_field('dica4') ?></p>
                      </div>
                    </div>
                  </div>

              </div>
            </div>

            <?php
            endwhile;
            else :
                ?>
            <p>Nada foi encontrado</p>
            <?php
            endif;
            ?>
        <?php wp_reset_query(); ?>

            
          </div>
        </section>

        <section id="club" class="clube" style="background-image: url(wp-content/themes/template-alfama/assets/img/bg-clube.png);" >
          <div class="container">
            <div class="row">
              <img src="wp-content/themes/template-alfama/assets/img/club.png" class="clube-caixa" alt="">
              <div class="col-sm-5">

              </div>

                    <div class="col-sm-6 card-bonus">
                        <img src="wp-content/themes/template-alfama/assets/img/logo-clube.png" class="logo-clube" alt="">
                        <p><?php the_field('descricao') ?></p>
                        <h3>BENEFÍCIOS</h3>
                        <ul>
                            <li><img src="wp-content/themes/template-alfama/assets/img/pin.png" class="pin" alt="pin"><?php the_field('beneficio_1') ?></li>
                            <li><img src="wp-content/themes/template-alfama/assets/img/pin.png" class="pin" alt="pin"><?php the_field('beneficio_2') ?></li>
                            <li><img src="wp-content/themes/template-alfama/assets/img/pin.png" class="pin" alt="pin"><?php the_field('beneficio_3') ?></li>
                            <li><img src="wp-content/themes/template-alfama/assets/img/pin.png" class="pin" alt="pin"><?php the_field('beneficio_4') ?></li>
                            <li><img src="wp-content/themes/template-alfama/assets/img/pin.png" class="pin" alt="pin"><?php the_field('beneficio_5') ?></li>
                        </ul>
                        <div class="d-flex mt-5">
                            <h2>INVESTIMENTO DO<br> VENATOR SING</h2>
                            <span>R$ <?php the_field('valor_club') ?></span>
                        </div>
                        <div class="d-flex-valor">
                            <a href="https://venator.goomer.app/" target="_blank">FAÇA O SEU PEDIDO <img src="wp-content/themes/template-alfama/assets/img/seta.png" class="" alt=""></a>
                            <p class="m-left font-11"><?php the_field('observacao') ?></p>
                        </div>
                    </div>

            </div>
          </div>
        </section>

        <section id="video">
           <div class="container">
             <div class="row">
               <div class="col-sm-10 offset-sm-1">
               
                  <div class="row fundo-video">
                        <div class="col-sm-7 pad-0">
                          <iframe width="100%" height="360" style="position:absolute;top:0;left:0;border-radius:20px;" src="<?php the_field('link_do_video') ?>" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div>
                        <div class="col-sm-5 box-text-video">
                            <h2><?php the_field('titulo_video') ?></h2>
                            <p><?php the_field('resumo_video') ?></p>
                            <a href="https://www.youtube.com/channel/UCV2NZO6T46A54t45q4rxTbw" target="_blank" class="btn-video">CONHEÇA NOSSO CANAL <img src="wp-content/themes/template-alfama/assets/img/pin-seta.png" class="seta-video" alt=""></a>
                        </div>
                    </div>

                 </div>
             </div>
           </div>
        </section>

        <!-- <section id="insta">
          <div id="instafeed" class="owl-carousel galeria-insta"></div>
        </section> -->

        
        <footer id="footer">
          <div class="container">
            <div class="row">
              <div class="col-sm-2">
                 <img src="wp-content/themes/template-alfama/assets/img/logo-footer.png" class="img-fluid" alt="">
              </div>
              <div class="col-sm-4">
                <h3>Venator Charutaria</h3>
                <p class="descri-footer">A Venator é a primeira Charutaria Cigar Lounge do Nordeste. Um lugar único que nasceu com a missão em desenvolver e divulgar a cultura do mundo do charuto, mantendo o compromisso em levar o mais alto padrão em qualidade, armazenamento e procedência dos seus produtos, proporcionando a experiência da nobre degustação do charuto a um público seleto e sofisticado.</p>
                <div class="d-flex social-mobile-footer">
                  <ul>
                    <li><a href="https://www.facebook.com/venatorcharutarialounge/" target="_blank"><img src="wp-content/themes/template-alfama/assets/img/face-footer.png" alt=""></a></li>
                    <li><a href="https://www.youtube.com/channel/UCV2NZO6T46A54t45q4rxTbw" target="_blank"><img src="wp-content/themes/template-alfama/assets/img/tube-footer.png" alt=""></a></li>
                    <li><a href="https://www.instagram.com/venatorcharutarialounge/" target="_blank"><img src="wp-content/themes/template-alfama/assets/img/insta-footer.png" alt=""></a></li>
                  </ul>
                </div>
              </div>
              <div class="col-sm-3 descri-2-footer">
                <h4>Loja Venator</h4>
                <p><img src="wp-content/themes/template-alfama/assets/img/local.png" alt="">Rua Vila Cristina, 130 Vila Manoel, Ljs 03 e 04. Aracaju/SE - CEP 49001500</p>
                <p><img src="wp-content/themes/template-alfama/assets/img/whats.png" alt=""> (79) 99650-7061</p>
                <p><img src="wp-content/themes/template-alfama/assets/img/email.png" alt=""> contato@venatorcharutaria.com.br</p>
              </div>
              <div class="col-sm-3 descri-2-footer">
                <h4>Ponto de vendas</h4>
                <p><img src="wp-content/themes/template-alfama/assets/img/local.png" alt="">Shopping Riomar (Próximo a Ricardo Almeida)</p>
                <p><img src="wp-content/themes/template-alfama/assets/img/whats.png" alt=""> (79) 99650-7061</p>
                
              </div>
            </div>
            <div class="row">
              <div class="col-sm-12">
                <hr>
              </div>
            </div>
            <div class="row copy">
              <div class="col-sm-10">
                <p>@Copyright 2021 - Todos os direitos reservados à Venator Charutaria</p>
              </div>
              <div class="col-sm-2">
                  <img src="wp-content/themes/template-alfama/assets/img/alfamaweb.png" alt="">
              </div>
            </div>
          </div>
        </footer>

          <a href="https://api.whatsapp.com/send?phone=5579996507061" class="float" target="_blank">
            <i class="fab fa-whatsapp my-float"></i>
          </a>
          <a href="https://venator.goomer.app/" class="float-2" target="_blank">
            <img src="wp-content/themes/template-alfama/assets/img/entrega.png" alt="">
          </a>




</div>
<?php get_footer(); ?>